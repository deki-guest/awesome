#!/bin/sh

set -e

cp -a tests "$AUTOPKGTEST_TMP"
cd "$AUTOPKGTEST_TMP/tests"

export CI=true
export AWESOME=/usr/bin/awesome
export AWESOME_CLIENT=/usr/bin/awesome-client
export AWESOME_THEMES_PATH=/usr/share/awesome/themes
export AWESOME_ICON_PATH=/usr/share/awesome/icons
export AWESOME_RC_FILE=/etc/xdg/awesome/rc.lua
export CMAKE_BINARY_DIR="$AUTOPKGTEST_TMP/tests"
export TEST_TIMEOUT=180
export XDG_CACHE_HOME="$AUTOPKGTEST_TMP/cache"

# test-gravity needs to be compiled first
gcc test-gravity.c -o test-gravity -lxcb -lxcb-icccm -lxcb-util

./run.sh
