Source: awesome
Section: x11
Priority: optional
Maintainer: Reiner Herrmann <reiner@reiner-h.de>
Build-Depends: asciidoctor,
               cmake,
               debhelper-compat (= 13),
               imagemagick,
               libcairo2-dev,
               libdbus-1-dev,
               libgdk-pixbuf-2.0-dev,
               libglib2.0-dev,
               liblua5.3-dev,
               libpango1.0-dev,
               libstartup-notification0-dev,
               libx11-xcb-dev,
               libxcb-cursor-dev,
               libxcb-icccm4-dev,
               libxcb-keysyms1-dev,
               libxcb-randr0-dev,
               libxcb-shape0-dev,
               libxcb-util-dev,
               libxcb-xinerama0-dev,
               libxcb-xkb-dev,
               libxcb-xrm-dev,
               libxcb-xtest0-dev,
               libxdg-basedir-dev,
               libxkbcommon-dev,
               libxkbcommon-x11-dev,
               lua-busted,
               lua-discount,
               lua-ldoc,
               lua-lgi (>= 0.9.2),
               lua5.3,
               x11proto-core-dev,
               xmlto,
               zsh
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://awesomewm.org/
Vcs-Git: https://salsa.debian.org/reiner/awesome.git
Vcs-Browser: https://salsa.debian.org/reiner/awesome

Package: awesome
Architecture: any
Recommends: feh, rlwrap, x11-xserver-utils, awesome-extra, gir1.2-gtk-3.0
Suggests: awesome-doc
Depends: default-dbus-session-bus | dbus-session-bus,
         gir1.2-freedesktop,
         gir1.2-gdkpixbuf-2.0,
         gir1.2-glib-2.0,
         gir1.2-pango-1.0,
         libcairo-gobject2,
         lua-lgi (>= 0.9.2),
         menu,
         ${misc:Depends},
         ${shlibs:Depends}
Provides: notification-daemon, x-window-manager
Description: highly configurable X window manager
 awesome manages windows dynamically in floating or tiled layouts. It is
 primarily targeted at power users, developers, and all those dealing with
 everyday computing tasks and looking for fine-grained control over their
 graphical environment.
 .
 It is highly extensible and scriptable via the Lua programming language,
 providing an easy-to-use and very well documented API to configure its
 behavior.
 .
 awesome uses tags instead of workspaces, which gives better flexibility
 in displaying windows, and can be entirely keyboard-driven, not needing a
 mouse. It also supports multi-headed configurations; uses XCB instead of
 Xlib for better performance; implements many freedesktop standards; and
 can be controlled over D-Bus from awesome-client.

Package: awesome-doc
Architecture: all
Section: doc
Multi-Arch: foreign
Depends: ${misc:Depends}
Replaces: awesome (<< 4.2-2)
Breaks: awesome (<< 4.2-2)
Description: highly configurable X window manager - documentation
 awesome manages windows dynamically in floating or tiled layouts. It is
 primarily targeted at power users, developers, and all those dealing with
 everyday computing tasks and looking for fine-grained control over their
 graphical environment.
 .
 It is highly extensible and scriptable via the Lua programming language,
 providing an easy-to-use and very well documented API to configure its
 behavior.
 .
 awesome uses tags instead of workspaces, which gives better flexibility
 in displaying windows, and can be entirely keyboard-driven, not needing a
 mouse. It also supports multi-headed configurations; uses XCB instead of
 Xlib for better performance; implements many freedesktop standards; and
 can be controlled over D-Bus from awesome-client.
 .
 This package contains the API documentation for awesome.
